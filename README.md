## After Login/Logout Redirection

A simple module providing a feature to redirect users according to an URL-defined
parameter after logging in or logout from the system. Allows redirect only internal url of site.

- For a full description of the module visit:
  https://www.drupal.org/project/alr

- To submit bug reports and feature suggestions, or to track changes visit:
  https://www.drupal.org/project/issues/alr


## CONTENTS OF THIS FILE

- Requirements
- Installation
- Configuration
- Maintainers

## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

- Install the Redirect after login module as you would normally install a
  contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
  further information.

## CONFIGURATION

1. After successfully installing the module After Login/Logout Redirection,
   you can add URL to each role of site using
   admin -> configuration -> system -> "Login/Logout module config settings" titled menu or
   go to /admin/config/alr-configuration.
2. Add a valid url to each roles

Note: You can use full url or url starting with '/' character

## MAINTAINERS

- Dharmendra Saini (dharmendra-virasat) - https://www.drupal.org/u/dharmendra-virasat

# Supporting organization

- Virasat Solutions - https://www.drupal.org/virasat-solutions

Expert in Drupal design, development, Theming, and migration, as well as UI/UX design which makes us
a one-stop Drupal development company.

